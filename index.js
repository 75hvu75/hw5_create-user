'use strict';

function createNewUser(userName, userSurName) {
    userName = prompt(`Введіть ваше ім'я`);
    userSurName = prompt(`Введіть ваше прізвище`);
    const newUser = {
        firstName: userName,
        lastName: userSurName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
    };
    return newUser;
};

const user = createNewUser();

console.log(user.getLogin());
